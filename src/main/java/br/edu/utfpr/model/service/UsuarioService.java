/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.model.service;

import br.edu.utfpr.model.Usuario;
import br.edu.utfpr.model.dao.UsuarioDAO;

/**
 *
 * @author anderson
 */
public class UsuarioService extends
        AbstractService<Long, Usuario> {

    public UsuarioService() {
        dao = new UsuarioDAO();
    }
}
