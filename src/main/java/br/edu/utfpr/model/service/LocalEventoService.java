/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.model.service;

import br.edu.utfpr.model.LocalEvento;
import br.edu.utfpr.model.dao.LocalEventoDAO;

/**
 *
 * @author anderson
 */
public class LocalEventoService extends AbstractService<Long, LocalEvento> {

    public LocalEventoService() {
        dao = new LocalEventoDAO();
    }

}
