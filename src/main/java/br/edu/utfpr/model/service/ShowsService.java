/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.model.service;

import br.edu.utfpr.model.Shows;
import br.edu.utfpr.model.dao.ShowsDAO;

/**
 *
 * @author anderson
 */
public class ShowsService extends AbstractService<Long, Shows> {

    public ShowsService() {
        dao = new ShowsDAO();
    }

}
