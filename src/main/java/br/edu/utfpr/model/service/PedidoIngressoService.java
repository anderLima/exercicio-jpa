/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.model.service;

import br.edu.utfpr.model.PedidoIngresso;
import br.edu.utfpr.model.dao.PedidoIngressoDAO;

/**
 *
 * @author anderson
 */
public class PedidoIngressoService extends AbstractService<Long, PedidoIngresso> {

    public PedidoIngressoService() {
        dao = new PedidoIngressoDAO();
    }

}
