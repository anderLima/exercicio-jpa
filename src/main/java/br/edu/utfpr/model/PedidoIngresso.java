/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author anderson
 */
@Entity
public class PedidoIngresso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int quantidade;
    private Long valorDoacao;
    @ManyToOne
    private Usuario usuario;
    @ManyToOne
    private Shows show;

    public PedidoIngresso() {
    }

    public PedidoIngresso(int quantidade, Long valorDoacao, Usuario usuario, Shows show) {
        this.quantidade = quantidade;
        this.valorDoacao = valorDoacao;
        this.usuario = usuario;
        this.show = show;
    }

    public Long getValorDoacao() {
        return valorDoacao;
    }

    public Shows getShow() {
        return show;
    }

    public void setShow(Shows show) {
        this.show = show;
    }

    public void setValorDoacao(Long valorDoacao) {
        this.valorDoacao = valorDoacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidoIngresso)) {
            return false;
        }
        PedidoIngresso other = (PedidoIngresso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String retorno = "id=" + id + " quantidade " + quantidade + " valor doação " + valorDoacao;
        if (usuario != null) {
            retorno += " Usuario " + usuario.getNome();
        }
        if (show != null) {
            retorno += " Show da Banda " + show.getBanda() + " Data da Show " + show.getData();
        }
        return retorno;
    }

}
