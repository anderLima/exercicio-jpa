/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.exercicio;

import br.edu.utfpr.model.PedidoIngresso;
import br.edu.utfpr.model.Usuario;
import br.edu.utfpr.model.LocalEvento;
import br.edu.utfpr.model.Shows;
import br.edu.utfpr.model.service.LocalEventoService;
import br.edu.utfpr.model.service.PedidoIngressoService;
import br.edu.utfpr.model.service.ShowsService;
import br.edu.utfpr.model.service.UsuarioService;
import com.mysql.fabric.xmlrpc.base.Data;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author anderson
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/main"})
public class MainServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MainServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MainServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            criarBD();
        } catch (ParseException ex) {
            Logger.getLogger(MainServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void criarBD() throws ParseException {

        LocalEventoService localEventoService = new LocalEventoService();

        LocalEvento lacerda = new LocalEvento(conversaoData("17/11/2017"), conversaoData("18/11/2017"), "Guarapuava");
        localEventoService.save(lacerda);

        ShowsService showsService = new ShowsService();

        Shows linkPark = new Shows(conversaoData("17/11/2017"), "Link Park", lacerda);
        Shows kiss = new Shows(conversaoData("18/11/2017"), "Kiss", lacerda);

        showsService.save(linkPark);
        showsService.save(kiss);

        UsuarioService usuarioService = new UsuarioService();

        Usuario anderson = new Usuario("Anderson", 24);
        Usuario joao = new Usuario("João", 25);
        Usuario pedro = new Usuario("Pedro", 20);

        usuarioService.save(anderson);
        usuarioService.save(joao);
        usuarioService.save(pedro);

        PedidoIngressoService pedidoIngressoService = new PedidoIngressoService();

        PedidoIngresso pedidoAnderson = new PedidoIngresso(2, Long.parseLong("500"), anderson, linkPark);
        PedidoIngresso pedido2Anderson = new PedidoIngresso(1, Long.parseLong("100"), anderson, kiss);
        PedidoIngresso pedidoJoao = new PedidoIngresso(4, Long.parseLong("1500"), joao, linkPark);
        PedidoIngresso pedidoPedro = new PedidoIngresso(1, Long.parseLong("50"), pedro, kiss);

        pedidoIngressoService.save(pedidoAnderson);
        pedidoIngressoService.save(pedido2Anderson);
        pedidoIngressoService.save(pedidoJoao);
        pedidoIngressoService.save(pedidoPedro);

    }

    private Date conversaoData(String value) throws ParseException {

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date data = formato.parse(value);
        return data;
    }
}
