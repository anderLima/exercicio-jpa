/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.utfpr.exercicio;

import br.edu.utfpr.model.PedidoIngresso;
import br.edu.utfpr.model.Usuario;

import java.io.PrintWriter;
import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author anderson
 */
@WebServlet(name = "ConsultaServlet", urlPatterns = {"/consultar"})
public class ConsultaServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConsultaServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConsultaServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        consultaBD();
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void consultaBD() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("disciplinas");
        EntityManager em = emf.createEntityManager();

        System.out.println("Lista de Usuarios:");
        Query q;
        q = em.createQuery("SELECT u FROM Usuario u ORDER BY u.nome");
        List<Usuario> listUsuario = q.getResultList();
        listUsuario.forEach((usuario) -> {
            System.out.println(usuario);
        });
        System.out.println("----------FIM-USUARIO------------");

        System.out.println("--------Pedido-Ingresso-----------");

        q = em.createQuery("SELECT p FROM PedidoIngresso p JOIN p.usuario u WHERE u.nome = :nome");
        q.setParameter("nome", "Anderson");

        List<PedidoIngresso> listPedido = q.getResultList();
        listPedido.forEach((pedido) -> {
            System.out.println(pedido);

        });
        System.out.println("-----FIM-Pedido-Ingresso---------");

        System.out.println("---------Usuario-Show------------");

        q = em.createQuery("SELECT p FROM PedidoIngresso p JOIN p.show s JOIN p.usuario u WHERE s.banda = :banda");
        q.setParameter("banda", "Link Park");

        List<PedidoIngresso> listShowUsuario = q.getResultList();
        listShowUsuario.forEach((show) -> {
            System.out.println(show);

        });
        System.out.println("-----FIM-Usuario-Show---------");

        System.out.println("--------MAIOR DOAÇÂO----------");

        q = em.createQuery("SELECT p FROM PedidoIngresso p JOIN p.show s "
                + "JOIN p.usuario u WHERE p.valorDoacao = "
                + "(SELECT MAX(sp.valorDoacao) FROM PedidoIngresso sp)");

        List<PedidoIngresso> maiorDoacao = q.getResultList();
        System.out.println(maiorDoacao);

        System.out.println("-------FIM-MAIOR DOAÇÂO--------");

        System.out.println("------Ingressos-Vendidos-------");
        q = em.createQuery("SELECT SUM(p.quantidade), s.banda FROM PedidoIngresso p JOIN p.show s GROUP BY s.banda");

        List<Object[]> ingressos = q.getResultList();
        ingressos.forEach((resultado) -> {
            System.out.println("Ingreços vendidos" + resultado[0] + " Banda " + resultado[1]
            );
        }
        );

    }
}
